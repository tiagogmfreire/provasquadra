<?php

namespace App\DAO;

use Illuminate\Database\Eloquent\Model;
use App\SistemaModel;

/**
 * Data Acess Object para a entidade de sistema.
 */
class SistemaDAO extends Model
{
    const ATIVO = 1;    
    
    /**
     * Método que faz a pesquisa por sistema.
     *
     * @param string $descricao Descrição do sistema
     * @param string $sigla     Sigla do sistema
     * @param string $email     E-mail do sistema
     * 
     * @return mixed Coleção com os sistemas encontrados
     */
    public function pesquisar($descricao, $sigla, $email)
    {
        
        if (empty($descricao) && empty($sigla) && empty($email)) {
            return [];
        }
        
        $resultado = SistemaModel::with('status');

        if (!empty($descricao)) {
            $resultado = $resultado->where('descricao', 'ILIKE', '%'.$descricao.'%');
        }
        
        if (!empty($sigla)) {
            $resultado = $resultado->where('sigla', 'ILIKE', '%'.$sigla.'%');
        }

        if (!empty($email)) {
            $resultado = $resultado->where('email', 'ILIKE', '%'.$email.'%');
        }

        $resultado = $resultado->get();

        return $resultado;
    }

    /**
     * Método para incluir sistema
     *
     * @param string $descricao Descrição do sistema
     * @param string $sigla     Sigla do sistema
     * @param string $email     E-mail do sistema
     * @param string $url       URL do sistema
     * @param int    $status    Status do sistema Default 1 (Ativo) 
     * 
     * @return mixed Resultado da operação
     */
    public function incluir($descricao, $sigla, $email, $url, $status = self::ATIVO)
    {
        $sistemaModel = new SistemaModel();

        $sistemaModel->descricao = $descricao;
        $sistemaModel->sigla = $sigla;
        $sistemaModel->email = $email;
        $sistemaModel->url = $url;
        $sistemaModel->status_id = $status;

        return $sistemaModel->save();
    }
}
