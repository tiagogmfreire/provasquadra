<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Modelo da tabela de domínio de status do sistema
 */
class StatusModel extends Model
{
    protected $table = 'tb_status';
    public $timestamps = false;
    protected $fillable = array('*');
    protected $guarded = ['id'];
}
