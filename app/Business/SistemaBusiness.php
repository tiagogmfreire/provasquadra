<?php

namespace App\Business;

use Illuminate\Database\Eloquent\Model;
use App\DAO\SistemaDAO;

/**
 * Classe de regra de negócio da entidade de sistema
 * usando padrão Business Object.
 */
class SistemaBusiness extends Model
{
    protected $sistemaDAO;

    const ATIVO = 1; 

    /**
     * Construtor que instancia a DAO.
     */
    public function __construct()
    {
        $this->sistemaDAO = new SistemaDAO();
    }

    /**
     * Método que realiza a pesquisa de sistema
     * com as informação fornecidas.
     *
     * @param string $descricao Descrição do sistema
     * @param string $sigla     Sigla do sistema
     * @param string $email     E-mail de contato do sistema
     * 
     * @return mixed Coleção contendo o resultado da pesquisa
     */
    public function pesquisar($descricao, $sigla, $email)
    {
        return $this->sistemaDAO->pesquisar($descricao, $sigla, $email);    
    }

    /**
     * Método que faz a inclusão de novo sistema
     *
     * @param string $descricao Descrição do sistema
     * @param string $sigla     Sigla do sistema
     * @param string $email     E-mail de contato
     * @param string $url       URL do sistema
     * @param int    $status    Status do sistema Default 1 (Ativo) 
     * 
     * @return mixed Resultado da operação.
     */
    public function incluir($descricao, $sigla, $email, $url, $status = self::ATIVO)
    {
        return $this->sistemaDAO->incluir($descricao, $sigla, $email, $url);
    }
}
