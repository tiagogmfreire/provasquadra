<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Modelo da entidade de sistema
 */
class SistemaModel extends Model
{
    protected $table = 'tb_sistema';
    public $timestamps = false;
    protected $fillable = array('*');
    protected $guarded = ['id'];

    public function status()
    {
        return $this->belongsTo('App\StatusModel', 'status_id')->withDefault();
    }
}
