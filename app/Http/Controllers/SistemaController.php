<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Business\SistemaBusiness;

/**
 * Classe de controler da funcionalidade de manter sistema
 */
class SistemaController extends Controller
{
    /**
     * Método que exibe a view de pesquisa   
     * 
     * @return view   Retorna a view de pesquisa.  
     */
    public function pesquisarView()
    {
        return view('pesquisar');
    }

    /**
     * Método que realiza a pesquisa.
     * 
     * Exibe a view de pesquisa com os resultados encotrados
     * (se houverem).
     *
     * @param Request $request
     * @return view   Retorna a view de pesquisa.
     */
    public function pesquisar(Request $request)
    {
        $descricao = $request->input('descricao');
        $sigla = $request->input('sigla');
        $email = $request->input('email');

        $sistemaBusiness = new SistemaBusiness();

        $sistemas = $sistemaBusiness->pesquisar($descricao, $sigla, $email);

        //var_dump($sistemas);

        return view('pesquisar')->with('sistemas', $sistemas);
    }

    /**
     * Método que exibe a view de inclusão de sistema.
     *
     * @return view   Retorna a view de incluir sistema.
     */
    public function incluirView()
    {
        return view('incluir');
    }

    /**
     * Método que realiza a inclusão do sistema.
     *
     * @param Request $request  
     * @return view   Retorna a view de incluir sistema.   
     */
    public function incluir(Request $request)
    {
        $descricao = $request->input('descricao');
        $sigla = $request->input('sigla');
        $email = $request->input('email');   
        $url = $request->input('url');   

        $msg = new \StdClass();
        
        $resultado = '';

        if (!empty($descricao) && !empty($sigla)) {

            $sistemaBusiness = new SistemaBusiness();

            $resultado = $sistemaBusiness->incluir($descricao, $sigla, $email, $url);

        } else {
            $msg->erro = 'Dados obrigatórios não informados';
        }

        if ($resultado) {
            $msg->sucesso = 'Operação Realizada com sucesso';
        }

        return view('incluir')->with('msg', $msg);
    }
}
