-- CREATE ROLE provasquadra WITH ENCRYPTED PASSWORD 'Pr0v4_squ4dr4';
-- ALTER ROLE provasquadra WITH LOGIN;
-- CREATE DATABASE provasquadra WITH OWNER provasquadra;
-- GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO provasquadra;
-- GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO provasquadra;

CREATE TABLE tb_status
(
	id 						SERIAL NOT NULL,
	descricao				VARCHAR(100),
	PRIMARY KEY(id)
);

CREATE TABLE tb_sistema 
(
	id 						SERIAL NOT NULL,
	descricao				VARCHAR(100),
	sigla					VARCHAR(10),
	email					VARCHAR(100),
	url						VARCHAR(50),
	status_id				INT REFERENCES tb_status(id),
	PRIMARY KEY(id)
);

INSERT INTO tb_status(descricao) VALUES('Ativo');
INSERT INTO tb_status(descricao) VALUES('Cancelado');


