<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', array('uses' => 'SistemaController@pesquisarView'));
Route::get('/incluir', array('uses' => 'SistemaController@incluirView'));

Route::post('/', array('uses' => 'SistemaController@pesquisar'));
Route::post('/incluir', array('uses' => 'SistemaController@incluir'));
