<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Pesquisar Sistema</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>        
    </head>
    <body>
        <div class="container">

            <div class="card">
                <div class="card-header">
                    <h2>Pesquisar Sistema</h2>
                </div>
                <div class="card-body">
                    <form method="post" action="/">
                    @csrf
                        <div class="form-group">
                            <label for="descricao">Descrição:</label>
                            <input type="text" class="form-control" id="descricao" name="descricao">
                        </div>
                        <div class="form-group">
                            <label for="sigla">Sigla:</label>
                            <input type="text" class="form-control" id="sigla" name="sigla">
                        </div>
                        <div class="form-group">
                            <label for="email">E-mail de atendimento do sistema:</label>
                            <input type="email" class="form-control" id="email" name="email">
                        </div>
                        
                        <button type="submit" class="btn btn-primary">                    
                            Pesquisar
                            <i class="fas fa-search"></i>
                        </button>
                        <button type="reset" class="btn btn-primary">
                            Limpar
                            <i class="fas fa-backspace"></i>
                        </button>
                        <button type="button" class="btn btn-primary" onClick="window.location = '/incluir'">
                            Novo Sistema
                            <i class="fas fa-plus-circle"></i>
                        </button>                
                    </form>

                    <br><br>

                    
                    </div>  
                    
                </div> 
                <div class="card-footer">
                    @if(isset($sistemas))
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Descrição</th>
                                    <th>Sigla</th>
                                    <th>E-mail de atendimento</th>
                                    <th>URL</th>
                                    <th>Status</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($sistemas as $sistema)
                                    <tr>
                                        <td>{{$sistema->descricao}}</td>
                                        <td>{{$sistema->sigla}}</td>
                                        <td>{{$sistema->email}}</td>
                                        <td>{{$sistema->url}}</td>
                                        <td>{{$sistema->status->descricao}}</td>
                                        <td><a href="#"><span><i class="fas fa-pencil-alt"></i></span></a></td>
                                    </tr>                                
                                @endforeach
                            </tbody>                    
                        </table>
                    @endif            
                </div>
            </div>

            
            
            
        <script>
            $(document).ready( function () {
                $('.table').DataTable({
                    "pageLength": 5,
                    language: {
                        url: 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json'
                    }
                });
            } );
        </script>      
    </body>
</html>
